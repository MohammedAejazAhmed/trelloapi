//import {posting} from "./application.js";
function index() {
  document.getElementById("List").addEventListener("click", listClick);
  document.getElementsByClassName("btn")[0].addEventListener("click", close);
  document.getElementsByTagName("button")[0].addEventListener("click", createBoard);
}
index();

function listClick() {
  document.getElementById("overlay").style.display = "flex";
  document.getElementById("overlay").style.justifyContent = "center";
  document.getElementById("overlay").style.alignItems = "center";
  document.getElementById("overlay").style.flexDirection = "row";
}

function close() {
  document.getElementById("overlay").style.display = "none";
}

function createBoard(){
  const value = document.getElementsByTagName("input")[0].value;
  posting(value);
}